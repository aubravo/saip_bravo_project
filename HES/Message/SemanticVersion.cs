﻿namespace Message
{
  public class SemanticVersion
  {
    public uint Major { get; set; }
    public uint Minor { get; set; }
    public uint Patch { get; set; }
  }
}
