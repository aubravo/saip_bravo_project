﻿namespace Message.Test
{
  public class TestMeterReading
  {
    public string MeterNumber { get; set; }
    public string Reading { get; set; }
  }
}
