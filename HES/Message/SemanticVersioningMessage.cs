﻿namespace Message
{
  public class SemanticVersioningMessage
  {
    public SemanticVersion ApiVersion { get; set; }
    public string MessageType { get; set; }
    public string Message { get; set; }
  }
}
