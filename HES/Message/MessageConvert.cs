﻿using System;
using Newtonsoft.Json;

namespace Message
{
  public class MessageConvert
  {
    public uint Major { get; set; }
    public uint Minor { get; set; }
    public uint Patch { get; set; }

    public string SerializeMessage<T>(T message)
    {
      SemanticVersioningMessage semanticVersioningMessage = new SemanticVersioningMessage
      {
        ApiVersion = new SemanticVersion {Major = Major, Minor = Minor, Patch = Patch},
        MessageType = typeof(T).Name,
        Message = JsonConvert.SerializeObject(message)
      };
      return JsonConvert.SerializeObject(semanticVersioningMessage);
    }

    public T DeserializeMessage<T>(string data, bool ignoreType = false)
    {
      try
      {
        SemanticVersioningMessage semanticVersioningMessage = 
          JsonConvert.DeserializeObject<SemanticVersioningMessage>(data);
        if (semanticVersioningMessage.ApiVersion.Major != Major)
        {
          Console.WriteLine("Wrong major version {0} expected {1}", 
            semanticVersioningMessage.ApiVersion.Major,
            Major);
          return default(T);
        }

        if (!ignoreType && semanticVersioningMessage.MessageType != typeof(T).Name)
        {
          Console.WriteLine("Wrong message type {0} expected {1}",
            semanticVersioningMessage.MessageType,
            typeof(T).Name);
          return default(T);
        }
        return JsonConvert.DeserializeObject<T>(semanticVersioningMessage.Message);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
      return default(T);
    }
  }
}
