﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AMQPClient;
using Common;
using Common.LocalCache;
using Common.messages;
using Message;
using Microsoft.Extensions.Hosting;

namespace FrontEndApiGateway.HostedServices
{

  public class GetFromQueueHostedService : IHostedService, IDisposable
  {
    private readonly ISimpleQueue<MeterReadingMessage> queue;

    public GetFromQueueHostedService(ISimpleQueue<MeterReadingMessage> queue)
    {
      this.queue = queue;
    }

    private RabbitMqClient client;

    private string GetContainerInfo()
    {
      return $"(FrontEndApiGateway APIVersion {SemanticApiVersion.Major}.{SemanticApiVersion.Minor}.{SemanticApiVersion.Patch} )";
    }


    private bool NewMessage(string data, string routingKey)
    {
      MessageConvert inputMessageConvert = new MessageConvert { Major = SemanticApiVersion.Major, Minor = SemanticApiVersion.Minor, Patch = SemanticApiVersion.Patch };
      var message = inputMessageConvert.DeserializeMessage<MeterReadingMessage>(data, true);
      if (message == null)
      {
        Console.WriteLine("Wrong message");
        return false;
      }

      string logMessage = " Message from " + message.MeterId + " " + GetContainerInfo();
      Console.WriteLine(logMessage);

      message.AddLogLine(GetContainerInfo());
      queue.Enqueue(message);
      return true;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
      do
      {
        try
        {
          client = new RabbitMqClient();
          client.SubscribeToMessage(QueueName.MeterReadingsFrontEndApiV1, NewMessage);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
          client?.Dispose();
          client = null;
          Thread.Sleep(5000);
        }
      } while (client == null);

      return Task.FromResult<object>(null);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
      client?.Dispose();
      return Task.FromResult<object>(null);
    }

    public void Dispose()
    {
      client?.Dispose();
    }
  }
}
