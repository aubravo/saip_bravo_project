﻿using System.Collections.Concurrent;

namespace Common.LocalCache
{
  public interface ISimpleQueue<T>
  {
    void Enqueue(T message);
    T Dequeue();
  }

  public class SimpleQueue<T> : ISimpleQueue<T>
  {
    private readonly ConcurrentQueue<T> queue = new ConcurrentQueue<T>();
    public void Enqueue(T message)
    {
      queue.Enqueue(message);
    }

    public T Dequeue()
    {
      if (queue.TryDequeue(out T message))
      {
        return message;
      }

      return default(T);
    }
  }

}
