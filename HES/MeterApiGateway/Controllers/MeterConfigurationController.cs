﻿using System.Diagnostics;
using System.Net;
using Common.LocalCache;
using Common.messages;
using Microsoft.AspNetCore.Mvc;

namespace MeterApiGateway.Controllers
{
  [Route("meterapigateway/[controller]")]
  [ApiController]
  public class MeterConfigurationController : ControllerBase
  {
    private readonly ISimpleQueue<MeterConfigMessage> localCache;

    public MeterConfigurationController(ISimpleQueue<MeterConfigMessage> localCache)
    {
      this.localCache = localCache;
    }

    // GET meterapigateway/<controller>
    [HttpGet]
    public ActionResult<MeterConfigMessage> Get()
    {
      var stopWatch = Stopwatch.StartNew();
      do
      {
        var messageFromQueue = localCache.Dequeue();

        if (messageFromQueue != null)
        {
          return Ok(messageFromQueue);
        }
      } while (stopWatch.Elapsed.TotalSeconds < 10);

      return StatusCode((int)HttpStatusCode.NotFound);
    }


  }
}
