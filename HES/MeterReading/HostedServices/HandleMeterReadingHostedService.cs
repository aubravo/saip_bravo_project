﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AMQPClient;
using Common;
using Common.messages;
using Message;
using Microsoft.Extensions.Hosting;

namespace MeterReading.HostedServices
{
  public class HandleMeterReadingHostedService : IHostedService, IDisposable
  {
    private RabbitMqClient client;
    private int delayMilliSeconds;

    public Task StartAsync(CancellationToken cancellationToken)
    {
      var testDelayMilliSeconds = Environment.GetEnvironmentVariable("TEST_DELAY");
      int.TryParse(testDelayMilliSeconds, out delayMilliSeconds);
      do
      {
        try
        {
          client = new RabbitMqClient();
          client.SubscribeToMessage(QueueName.MeterReadingsMeterApiV1, NewMessage);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
          client?.Dispose();
          client = null;
          Thread.Sleep(5000);
        }
      } while (client == null);

      return Task.FromResult<object>(null);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
      client?.Dispose();
      return Task.FromResult<object>(null);
    }

    private string GetContainerInfo()
    {
      return $"(MeterReading APIVersion {SemanticApiVersion.Major}.{SemanticApiVersion.Minor}.{SemanticApiVersion.Patch} Container ID {Environment.MachineName} )";
    }

    private bool NewMessage(string rawMessage, string routingKey)
    {
      if (delayMilliSeconds > 0)
      {
        Thread.Sleep(delayMilliSeconds);
      }
      MessageConvert inputMessageConvert = new MessageConvert { Major = SemanticApiVersion.Major, Minor = SemanticApiVersion.Minor, Patch = SemanticApiVersion.Patch };
      var inputmessage = inputMessageConvert.DeserializeMessage<MeterReadingMessage>(rawMessage, true);
      if (inputmessage == null)
      {
        Console.WriteLine("Bad message");
        return false;
      }

      string logMessage = " Message from " + inputmessage.MeterId + " " + GetContainerInfo();
      Console.WriteLine(logMessage);

      inputmessage.AddLogLine(GetContainerInfo());

      MessageConvert outputMessageConvert = new MessageConvert { Major = SemanticApiVersion.Major, Minor = SemanticApiVersion.Minor, Patch = SemanticApiVersion.Patch };
      string newMessage = outputMessageConvert.SerializeMessage(inputmessage);

      return client.SendMessage(newMessage, RoutingKey.MeterReadingsFrontEndApiV1);
    }

    public void Dispose()
    {
      client?.Dispose();
    }
  }
}
