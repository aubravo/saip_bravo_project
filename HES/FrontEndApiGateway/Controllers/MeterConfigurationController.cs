﻿using System;
using System.Net;
using AMQPClient;
using Common;
using Common.messages;
using Message;
using Microsoft.AspNetCore.Mvc;

namespace FrontEndApiGateway.Controllers
{
  [Route("frontendapigateway/[controller]")]
  [ApiController]
  public class MeterConfigurationController : ControllerBase
  {
    private RabbitMqClient client;
    public MeterConfigurationController()
    {
      if (client == null)
      {
        client = new RabbitMqClient();
      }
    }
    // POST api/<controller>
    [HttpPost]
    public IActionResult Post([FromBody]MeterConfigMessage message)
    {
      try
      {
        string logMessage = " Message from " + message.MeterId + " " + GetContainerInfo();
        Console.WriteLine(logMessage);
        message.AddLogLine(GetContainerInfo());
        MessageConvert outputMessageConvert = new MessageConvert { Major = SemanticApiVersion.Major, Minor = SemanticApiVersion.Minor, Patch = SemanticApiVersion.Patch };
        string newMessage = outputMessageConvert.SerializeMessage(message);

        client.SendMessage(newMessage, RoutingKey.MeterConfigurationFrontEndApiV1);
        return Ok();
      }
      catch (Exception)
      {
        client.Dispose();
        client = null;
        return StatusCode((int)HttpStatusCode.InternalServerError);
      }
    }
    private string GetContainerInfo()
    {
      return $"(FrontEndApiGateway APIVersion {SemanticApiVersion.Major}.{SemanticApiVersion.Minor}.{SemanticApiVersion.Patch} )";
    }

  }
}
