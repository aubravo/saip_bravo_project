﻿using System;
using System.Diagnostics;
using System.Net;
using Common.LocalCache;
using Common.messages;
using Microsoft.AspNetCore.Mvc;

namespace FrontEndApiGateway.Controllers
{
  [Route("frontendapigateway/[controller]")]
  [ApiController]
  public class MeterReadingController : ControllerBase
  {
    private readonly ISimpleQueue<MeterReadingMessage> localCache;

    public MeterReadingController(ISimpleQueue<MeterReadingMessage> localCache)
    {
      this.localCache = localCache;
    }
    // GET frontendapigateway/MeterReading
    [HttpGet]
    public ActionResult<MeterReadingMessage> Get()
    {
      var stopWatch = Stopwatch.StartNew();
      do
      {
        var messageFromQueue = localCache.Dequeue();

        if (messageFromQueue != null)
        {
          return Ok(messageFromQueue);
        }
      } while (stopWatch.Elapsed.TotalSeconds < 10);

      return StatusCode((int)HttpStatusCode.NotFound);
    }
  }
}
