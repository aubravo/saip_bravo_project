﻿namespace MeterConfigurationMiniorVersionOne
{
  public static class SemanticApiVersion
  {
    public const uint Major = 1;
    public const uint Minor = 1;
    public const uint Patch = 0;
  }
}
