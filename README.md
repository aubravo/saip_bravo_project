##How to run the application
The Application is based on Docker containers running on Linux.  
Remark: The application has only been tested on Microsoft Windows 10 Pro.  
The docker-compose file (docker-compose.yml) can be used when running the application using docker-compose from the command line.  
1.	Place the docker-compose file in the filesystem.  
2.	Run the command docker-compose up, at same location as docker-compose file.  
3.	Now Docker compose will pull all the images from Docker Hub.   
4.	Docker compose will start the application.  
Remark: the Docker images from the project is located at:  
https://hub.docker.com/u/saipteambravo  