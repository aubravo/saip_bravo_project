﻿namespace Common
{
  public static class RoutingKey
  {
    public const string MeterReadingsMeterApiV1 = "meterreadingsmeterapi.v1"; 
    public const string MeterReadingsFrontEndApiV1 = "meterreadingsfrontendapi.v1"; 
    public const string MeterConfigurationFrontEndApiV1 = "meterconfigurationfrontendapi.v1"; 
    public const string MeterConfigurationMeterApiV1 = "meterconfigurationmeterapi.v1"; 

  }
}
