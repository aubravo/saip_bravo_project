﻿using System.Collections;

namespace Message.Test
{
  public class TestMessageWithList
  {
    public string Id { get; set; }
    public IList LogLines { get; set; }
  }
}
