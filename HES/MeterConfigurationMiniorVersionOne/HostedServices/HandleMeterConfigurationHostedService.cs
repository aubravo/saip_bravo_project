﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AMQPClient;
using Common;
using Common.messages;
using Message;
using Microsoft.Extensions.Hosting;

namespace MeterConfigurationMiniorVersionOne.HostedServices
{
  public class HandleMeterConfigurationHostedService : IHostedService, IDisposable
  {
    private RabbitMqClient client;
    private int delayMilliSeconds;

    public Task StartAsync(CancellationToken cancellationToken)
    {
      var testDelayMilliSeconds = Environment.GetEnvironmentVariable("TEST_DELAY");
      int.TryParse(testDelayMilliSeconds, out delayMilliSeconds);

      do
      {
        try
        {
          client = new RabbitMqClient();
          client.SubscribeToMessage(QueueName.MeterConfigurationFrontEndApiV1, NewMessage);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
          client?.Dispose();
          client = null;
          Thread.Sleep(5000);
        }
      } while (client == null);

      return Task.FromResult<object>(null);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
      client?.Dispose();
      return Task.FromResult<object>(null);
    }

    private string GetContainerInfo()
    {
      return $"(MeterConfigurationMiniorVersionOne APIVersion {SemanticApiVersion.Major}.{SemanticApiVersion.Minor}.{SemanticApiVersion.Patch} Container ID {Environment.MachineName} )";
    }

    private bool NewMessage(string rawMessage, string routingKey)
    {
      if (delayMilliSeconds > 0)
      {
        Thread.Sleep(delayMilliSeconds);
      }
      MessageConvert inputMessageConvert = new MessageConvert
      {
        Major = SemanticApiVersion.Major, //1
        Minor = SemanticApiVersion.Minor, //1 this one has been rased by one
        Patch = SemanticApiVersion.Patch  //0
      };
      var message = inputMessageConvert.DeserializeMessage<MeterConfigMessage2>(rawMessage, true);
      if (message == null)
      {
        Console.WriteLine("Bad message");
        return false;
      }
      if (message.NewField == default(string))
      {
        string defaultMessage = "Newfield is default value setting value to '42'";
        Console.WriteLine(defaultMessage);
        message.AddLogLine(defaultMessage);
        message.NewField = "42";
      }
      string logMessage = " Message from " + message.MeterId + " " + GetContainerInfo();
      Console.WriteLine(logMessage);
      message.AddLogLine(GetContainerInfo());
      MessageConvert outputMessageConvert = new MessageConvert {
        Major = SemanticApiVersion.Major,
        Minor = SemanticApiVersion.Minor,
        Patch = SemanticApiVersion.Patch };
      string newMessage = outputMessageConvert.SerializeMessage(message);
      return client.SendMessage(newMessage, RoutingKey.MeterConfigurationMeterApiV1);
    }

    public void Dispose()
    {
      client?.Dispose();
    }
  }
}
