﻿using System;
using System.Net;
using AMQPClient;
using Common;
using Common.messages;
using Message;
using Microsoft.AspNetCore.Mvc;

namespace MeterApiGateway.Controllers
{

  [Route("meterapigateway/[controller]")]
  [ApiController]
  public class MeterReadingController : ControllerBase
  {

    private static RabbitMqClient client;

    public MeterReadingController()
    {
      if (client == null)
      {
        client = new RabbitMqClient();
      }
    }

    // POST <controller>
    [HttpPost]
    public IActionResult Post([FromBody]MeterReadingMessage message)
    {
      try
      {
        string logMessage = " Message from " + message.MeterId + " " + GetContainerInfo();
        Console.WriteLine(logMessage);
        message.AddLogLine(GetContainerInfo());
        MessageConvert outputMessageConvert = new MessageConvert { Major = SemanticApiVersion.Major, Minor = SemanticApiVersion.Minor, Patch = SemanticApiVersion.Patch };
        string newMessage = outputMessageConvert.SerializeMessage(message);
        client.SendMessage(newMessage, RoutingKey.MeterReadingsMeterApiV1);
        return Ok();

      }
      catch (Exception)
      {
        client.Dispose();
        client = null;
        return StatusCode((int) HttpStatusCode.InternalServerError);
      }
    }

    private string GetContainerInfo()
    {
      return $"(MeterApiGateway APIVersion {SemanticApiVersion.Major}.{SemanticApiVersion.Minor}.{SemanticApiVersion.Patch} )";
    }

  }
}
