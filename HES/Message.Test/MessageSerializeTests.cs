using System;
using NUnit.Framework;

namespace Message.Test
{
  [TestFixture]
  [Category("Unit Tests")]
  public class MessageSerializeTests
  {
    [Test]
    public void SerializeMessage_DeserializeMessage()
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF"};
      var message = new MessageConvert {Major = 1, Minor = 0, Patch = 0}.SerializeMessage(messageToSend);

      var messageToReceive = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.DeserializeMessage<TestMeterReading>(message);

      Assert.That(messageToReceive.MeterNumber, Is.EqualTo(messageToSend.MeterNumber));
      Assert.That(messageToReceive.Reading, Is.EqualTo(messageToSend.Reading));
    }

    [Test]
    public void SerializeMessage_DeserializeMessage_WrongType()
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF" };
      var message = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.SerializeMessage(messageToSend);

      var messageToRecive = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.DeserializeMessage<TestMeterReading2>(message);

      Assert.That(messageToRecive, Is.Null);
    }


    [TestCase((uint)2, (uint)3)]
    [TestCase((uint)2, (uint)1)]
    public void SerializeMessage_DeserializeMessage_WrongMajor(uint receiveMajor, uint sendMajor)
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF" };
      var message = new MessageConvert { Major = sendMajor, Minor = 0, Patch = 0 }.SerializeMessage(messageToSend);

      var messageToRecive = new MessageConvert { Major = receiveMajor, Minor = 0, Patch = 0 }.DeserializeMessage<TestMeterReading>(message);

      Assert.That(messageToRecive, Is.Null);
    }

    [TestCase((uint)2, (uint)3)]
    [TestCase((uint)2, (uint)1)]
    [TestCase((uint)2, (uint)2)]
    public void SerializeMessage_DeserializeMessage_DifferentMinior(uint receiveMinior, uint sendMinior)
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF" };
      var message = new MessageConvert { Major = 1, Minor = sendMinior, Patch = 0 }.SerializeMessage(messageToSend);

      var messageToReceive = (new MessageConvert { Major = 1, Minor = receiveMinior, Patch = 0 }).DeserializeMessage<TestMeterReading>(message);

      Assert.That(messageToReceive.MeterNumber, Is.EqualTo(messageToSend.MeterNumber));
      Assert.That(messageToReceive.Reading, Is.EqualTo(messageToSend.Reading));
    }

    [TestCase((uint)2, (uint)3)]
    [TestCase((uint)2, (uint)1)]
    [TestCase((uint)2, (uint)2)]
    public void SerializeMessage_DeserializeMessage_DifferentPatch(uint receivePatch, uint sendPatch)
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF" };
      var message = new MessageConvert { Major = 1, Minor = 1, Patch = sendPatch }.SerializeMessage(messageToSend);

      var messageToReceive = new MessageConvert { Major = 1, Minor = 1, Patch = receivePatch }.DeserializeMessage<TestMeterReading>(message);

      Assert.That(messageToReceive.MeterNumber, Is.EqualTo(messageToSend.MeterNumber));
      Assert.That(messageToReceive.Reading, Is.EqualTo(messageToSend.Reading));
    }

    [Test]
    public void SerializeMessageNewType_DeserializeMessageOldType_MiniorVersionRaise()
    {
      TestMeterReading2 messageToSend = new TestMeterReading2 { MeterNumber = "1234", Reading = "ABCDEF", TimeStamp = DateTime.Parse("2012-03-19T07:22Z")};
      // ReSharper disable once RedundantTypeArgumentsOfMethod
      var message = new MessageConvert { Major = 1, Minor = 1, Patch = 0 }.SerializeMessage<TestMeterReading2>(messageToSend);

      var messageToReceive = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.DeserializeMessage<TestMeterReading>(message, ignoreType:true);

      Assert.That(messageToReceive.MeterNumber, Is.EqualTo(messageToSend.MeterNumber));
      Assert.That(messageToReceive.Reading, Is.EqualTo(messageToSend.Reading));
    }

    [Test]
    public void SerializeMessageOldType_DeserializeMessageNewType_MiniorVersionFall()
    {
      TestMeterReading messageToSend = new TestMeterReading { MeterNumber = "1234", Reading = "ABCDEF"};
      // ReSharper disable once RedundantTypeArgumentsOfMethod
      var message = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.SerializeMessage<TestMeterReading>(messageToSend);

      var messageToReceive = new MessageConvert { Major = 1, Minor = 1, Patch = 0 }.DeserializeMessage<TestMeterReading2>(message, ignoreType: true);

      Assert.That(messageToReceive.MeterNumber, Is.EqualTo(messageToSend.MeterNumber));
      Assert.That(messageToReceive.Reading, Is.EqualTo(messageToSend.Reading));
      Assert.That(messageToReceive.TimeStamp, Is.EqualTo(default(DateTime)));
    }

    [Test]
    public void SerializeMessageWithArray_DeserializeMessageWithArray()
    {
      TestMessageWithList messageToSend = new TestMessageWithList
      {
        Id = "42",
        LogLines = new string[] {"Hallo", "World"}
      };

      var message = new MessageConvert { Major = 1, Minor = 0, Patch = 0 }.SerializeMessage<TestMessageWithList>(messageToSend);

      var messageToReceive = new MessageConvert { Major = 1, Minor = 1, Patch = 0 }.DeserializeMessage<TestMessageWithList>(message, ignoreType: true);

      Assert.That(messageToReceive.Id, Is.EqualTo(messageToSend.Id));
      Assert.That(messageToReceive.LogLines, Is.EqualTo(messageToSend.LogLines));
    }


  }
}