﻿using System.Collections.Generic;

namespace Common.messages
{
  public class MeterConfigMessage
  {
    public string MeterId { get; set; }
    public string Config { get; set; }
    public string[] LogLines { get; set; }

    public void AddLogLine(string logLine)
    {
      if (LogLines == null)
      {
        LogLines = new string[0];
      }
      LogLines = new List<string>(LogLines)
      {
        logLine
      }.ToArray();
    }

    public override string ToString()
    {
      return string.Format("MeterId {0} Data {1}, ConfigString {2}", MeterId, Config, string.Join(", ", LogLines));
    }

  }
}
