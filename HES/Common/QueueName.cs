﻿namespace Common
{
  public static class QueueName
  {
    public const string MeterReadingsMeterApiV1 = "MeterreadingsMeterApi.V1";
    public const string MeterReadingsFrontEndApiV1 = "MeterreadingsFrontendApi.V1";
    public const string MeterConfigurationFrontEndApiV1 = "MeterconfigurationFrontendApi.V1";
    public const string MeterConfigurationMeterApiV1 = "MeterconfigurationMeterApi.V1"; 
  }

  

}
