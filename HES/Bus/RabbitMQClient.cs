﻿using System;
using System.Collections.Generic;
using System.Text;
using Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace AMQPClient
{
  public class RabbitMqClient : IDisposable
  {

    private const string ExchangeName = "TopicExchange";

    private ConnectionFactory factory;
    private IConnection connection;
    private IModel channel;
    private readonly IDictionary<string, object> arguments;

    public RabbitMqClient()
    {
      var envQueueLength = Environment.GetEnvironmentVariable("RABBITMQ_QUEUE_LENGTH");

      if (!int.TryParse(envQueueLength, out var queueLength))
      {
        queueLength = 1000;
      }

      arguments = new Dictionary<string, object> { { "x-max-length", queueLength } };
      CreateConnection();
    }

    private void CreateConnection()
    {
      var hostName = Environment.GetEnvironmentVariable("RABBITMQ_HOST");
      var password = Environment.GetEnvironmentVariable("RABBITMQ_PASSWORD");
      var userName = Environment.GetEnvironmentVariable("RABBITMQ_USERNAME");

      factory = new ConnectionFactory { HostName = hostName, UserName = userName, Password = password };
      connection = factory.CreateConnection();
      channel = connection.CreateModel();
      channel.ExchangeDeclare(ExchangeName, "topic");
      channel.QueueDeclare(QueueName.MeterConfigurationFrontEndApiV1, false, false, false, arguments);
      channel.QueueDeclare(QueueName.MeterConfigurationMeterApiV1, false, false, false, arguments);
      channel.QueueDeclare(QueueName.MeterReadingsFrontEndApiV1, false, false, false, arguments);
      channel.QueueDeclare(QueueName.MeterReadingsMeterApiV1, false, false, false, arguments);

      channel.QueueBind(QueueName.MeterConfigurationFrontEndApiV1, ExchangeName, RoutingKey.MeterConfigurationFrontEndApiV1);
      channel.QueueBind(QueueName.MeterConfigurationMeterApiV1, ExchangeName, RoutingKey.MeterConfigurationMeterApiV1);
      channel.QueueBind(QueueName.MeterReadingsFrontEndApiV1, ExchangeName, RoutingKey.MeterReadingsFrontEndApiV1);
      channel.QueueBind(QueueName.MeterReadingsMeterApiV1, ExchangeName, RoutingKey.MeterReadingsMeterApiV1);
    }

    public bool SendMessage(string message, string routingKey)
    {
      channel.BasicPublish(ExchangeName, routingKey, null, Encoding.UTF8.GetBytes(message));
      return true;
    }

    public void SubscribeToMessage(string queueName, Func<string,string,bool> callBackAction)
    {
      var consumer = new EventingBasicConsumer(channel);
      consumer.Received += (ch, ea) =>
      {
        if (callBackAction(Encoding.UTF8.GetString(ea.Body), ea.RoutingKey))
        {
          channel.BasicAck(ea.DeliveryTag, false);
        }
      }; 
      channel.BasicConsume(queueName,false,consumer);
    }

    public void Dispose()
    {
      connection?.Close();
    }
  }
}
